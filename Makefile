procesador: obj/base.o
	gcc -Wall -pthread obj/base.o -lpng -o bin/procesador

obj/base.o: src/base.c
	gcc -pthread base.c -lpng -o obj/base.o

.PHONY: clean
clean:
	rm bin/* obj/*.o
